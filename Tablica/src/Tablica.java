public class Tablica {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int value = 8,
                size = 12;

        int[] tab = new int [size];
        tab[0] = value;
        for (int i = 1; i < size; i++)
            tab[i] = tab[i-1] + value;

        int j = 0,
                k = 1;
        while(j < tab.length)
        {
            System.out.println("Wartość komórki " + k + " wynosi " + tab[j]);
            j++;
            k++;
        }
    }
}
