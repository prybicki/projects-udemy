import java.util.Scanner;

public class Butelka {
    private double ileLitrow;
    private double maxPojemnosc;

    private Butelka(double ileLitrow, double maxPojemnosc) {
        this.ileLitrow = ileLitrow;
        this.maxPojemnosc = maxPojemnosc;
    }

    public static void main(String[] args) {

        int liczbaButelek;
        double ileWlac;
        double ileWylac;

        Scanner odczyt = new Scanner(System.in);

        System.out.println("Witaj w programie służącym do sprawdzania pojemności butelek.");
        System.out.println("Podaj liczbę posiadanych butelek?");
        liczbaButelek = odczyt.nextInt();
        Butelka[] butelka = new Butelka[liczbaButelek];

        for (int i = 0; i < liczbaButelek; i++) {
            System.out.println("Jaka jest pojemność " + (i + 1) + " butelki?");
            double maxPojemnosc = odczyt.nextDouble();
            System.out.println("Ile litrów wody jest w " + (i + 1) + " butelce?");
            double ileLitrow = odczyt.nextDouble();
            if (ileLitrow <= maxPojemnosc) {
                butelka[i] = new Butelka(ileLitrow, maxPojemnosc);
            } else {
                butelka[i] = new Butelka(maxPojemnosc, maxPojemnosc);
                System.out.println("Do butelki nie można wlać więcej wody niż wynosi jej objętość!");
                System.out.println("Wlano tylko " + butelka[i].getIleLitrow() + " litrów.");
            }
        }

        for (int i = 0; i < liczbaButelek; i++) {
            System.out.println("W butelce numer " + (i + 1) + " jest " + butelka[i].getIleLitrow() + " litrów. Pojemność butelki wynosi " + butelka[i].getmaxPojemnosc() + " litrów.");
        }

        System.out.println("----------MENU PROGRAMU-----------");
        System.out.println("-Wybierz opcje:                  -");
        System.out.println("-1. Dolej wodę                   -");
        System.out.println("-2. Wylej wodę                   -");
        System.out.println("-3. Przelej wodę miedzy butelkami-");
        System.out.println("----------------------------------");

        int wybor = odczyt.nextInt();
        int i; // numer butelki na której będzie prowadzone działanie
        int k; // do której butelki jest przelewana woda

        switch (wybor) {
            case 1:
                System.out.println("Do której butelki chcesz wlać wodę?");
                i = odczyt.nextInt() - 1;
                System.out.println("Ile chcesz wlać wody do butelki nr " + (i + 1) + "?");
                ileWlac = odczyt.nextDouble();
                butelka[i].wlej(ileWlac);
                for (int j = 0; j < liczbaButelek; j++) {
                    System.out.println("W butelce " + (j + 1) + " jest " + butelka[j].getIleLitrow() + "/" + butelka[j].getmaxPojemnosc() + " litrów.");
                }
                break;
            case 2:
                System.out.println("Z której butelki chcesz wylać wodę?");
                i = odczyt.nextInt() - 1;
                System.out.println("Ile chcesz wylać wody z butelki nr " + (i + 1) + "?");
                ileWylac = odczyt.nextDouble();
                butelka[i].wylej(ileWylac);
                for (int j = 0; j < liczbaButelek; j++) {
                    System.out.println("W butelce " + (j + 1) + " jest " + butelka[j].getIleLitrow() + "/" + butelka[j].getmaxPojemnosc() + " litrów.");
                }
                break;
            case 3:
                System.out.println("Z której butelki chcesz wylać wodę?");
                i = odczyt.nextInt() - 1;
                System.out.println("Do której butelki chcesz wlać wodę?");
                k = odczyt.nextInt() - 1;
                System.out.println("Ile chcesz wylać wody z butelki nr " + (i + 1) + " do butelki nr " + (k + 1) + "?");
                ileWylac = odczyt.nextDouble();
                ileWlac = ileWylac;
                butelka[i].wylej(ileWylac);
                butelka[k].wlej(ileWlac);
                for (int j = 0; j < liczbaButelek; j++) {
                    System.out.println("W butelce " + (j + 1) + " jest " + butelka[j].getIleLitrow() + "/" + butelka[j].getmaxPojemnosc() + " litrów.");
                }
                break;
            default:
                System.out.println("Nie wybrano żadnej opcji.");
        }
    }

    private double getIleLitrow() {
        return ileLitrow;
    }

    private double getmaxPojemnosc() {
        return maxPojemnosc;
    }

    // Metoda służąca do wlewania wody do butelki.
    private void wlej(double ilosc) {
        double maxWlej = getmaxPojemnosc() - this.getIleLitrow();
        if (ilosc <= maxWlej) {
            this.ileLitrow += ilosc;
            System.out.println("Do butelki wlano " + ilosc + " litrów. Po dolaniu wody w butelce jest " + this.getIleLitrow() + " litrów.");
        } else {
            double pozostalo = this.getIleLitrow() + ilosc - getmaxPojemnosc();
            System.out.println("W butelce nie ma miejsca na dolanie " + ilosc + " litrów. Wlano tylko " + maxWlej + " litrów. " + pozostalo + " litrów nie zmieściło się w butelce.");
            this.ileLitrow = getmaxPojemnosc();
        }
    }

    // Metoda służąca wylewaniu wody z butelki.
    private void wylej(double ilosc) {
        double maxWylej = this.getIleLitrow();
        if (ilosc <= maxWylej) {
            this.ileLitrow -= ilosc;
        } else {
            double zabraklo = maxWylej - this.getIleLitrow();
            System.out.println("Z butelki nie da się wylać " + ilosc + " litrów. Wylano tylko " + this.getIleLitrow() + " litrów. Nie dało się wylać " + zabraklo + " litrów wody.");
            this.ileLitrow = 0;
        }
    }
}
